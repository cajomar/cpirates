#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <ink.h>
#include <ink_data.h>
#include <ink_runner.h>
#include <lib2d.h>

#define STB_DS_IMPLEMENTATION
#include "stb_ds.h"
#include "map.h"
#include "game.h"
#include "idents.h"
#include "render.h"

extern float ship_turn_speeds[SHIP_TYPE_END];

struct ink* ink = 0;
Map* map = 0;
int viewport[4] = {0, 0, 0, 0};

static inkd_listener* main_listener = 0;
static inkd_listener* tweak_listener = 0;


void post_init(void) {
    if (lib2d_init(LIB2D_RENDER_BACKEND_GL, NULL) != 0) {
        fprintf(stderr, "lib2d_init failed: %s\n", lib2d_get_error());
        exit(EXIT_FAILURE);
    }
    render_init();
}

void pre_step(void) {
    ink_getViewport(ink, viewport+2, viewport+3);
    lib2d_viewport(viewport[2], viewport[3]);
    if (map) {
        render(map);
        lib2d_render();
    }
}

void post_step(float dt) {
    struct ink_data_event ev;
    while (inkd_listener_poll(main_listener, &ev)) {
        switch (ev.type) {
            case INK_DATA_EVENT_CHANGE:
                switch (ev.field) {
                    case I_tweaker:
                    case I_in_game:
                        if (inkd_get_bool(ev.data, ev.field)) {
                            assert(!map);
                            map = map_create();
                        } else {
                            assert(map);
                            map_destroy(map);
                            map = 0;
                        }
                        break;
                }
                break;
            case INK_DATA_EVENT_IMPULSE:
                switch (ev.field) {
                    case I_quit:
                        ink_runner_exit();
                        break;
                }
                break;
            default:
                break;
        }
    }
    while (inkd_listener_poll(tweak_listener, &ev)) {
        if (ev.type == INK_DATA_EVENT_CHANGE) {
            switch (ev.field) {
                case I_size:
                    map_destroy(map);
                    map = map_create();
                    break;
                case I_divisor:
                case I_lacunarity:
                case I_gain:
                case I_sea_level:
                case I_octaves:
                    generate_map(map);
                    break;
            }
        }
    }
    if (map) {
        game_step(map, dt);
    }
}


void unhandled_event(const struct ink_event* ev) {
    if (ev->event_type == INK_EVENT_TYPE_ACTION && 
            ev->data.action.action == INK_EVENT_ACTION_K_ESCAPE) {
        ink_runner_exit();
    }
    if (game_event(map, ev)) return;
}


int main(int argc, char* argv[]) {
    ink = ink_new(0);

    main_listener = inkd_listener_new(0);
    inkd source = ink_source(ink, I_main);
    inkd_add_listener(source, main_listener);
    tweak_listener = inkd_listener_new(0);
    inkd s = ink_source(ink, I_map_tweaker);
    inkd_add_listener(s, tweak_listener);

    inkd_int(s, I_size, 128);
    inkd_int(s, I_octaves, 4);
    inkd_float(s, I_lacunarity, 2.1f);
    inkd_float(s, I_gain, 0.55f);
    inkd_float(s, I_divisor, 32.f);
    inkd_float(s, I_sea_level, 0.55f);

    for (int i=0; i<argc; i++) {
        if (strcmp(argv[i], "-p") == 0 && map == NULL) {
            map = map_create();
            inkd_bool(ink_source(ink, I_main), I_in_game, true);
        } else if (strcmp(argv[i], "-t") == 0 && map == NULL) {
            map = map_create();
            inkd_bool(ink_source(ink, I_main), I_tweaker, true);
        }
    }

    struct ink_runner_options opts = {
        .window_title = "The Sea-Dog's Grandson",
        .gui_bin_path = "gui/gui.bin",
        .assets_bin_path = "gui/gui_assets.bin",
        .net_path = "gui/runner.ipc",
        .post_init = post_init,
        .pre_step = pre_step,
        .post_step = post_step,
        .unhandled_event = unhandled_event,
        .continuous_step = true,
    };
    ink_runner(&opts);
    inkd_listener_delete(main_listener);
    render_shutdown();
    lib2d_shutdown();
    return EXIT_SUCCESS;
}
