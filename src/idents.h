#ifndef INK_IDENTS_GEN
#define INK_IDENTS_GEN
#define I_Text 406401448860806331llu // Text
#define I_divisor 4722803603957995665llu // divisor
#define I_gain 1493880558539965944llu // gain
#define I_game 245837302133227191llu // game
#define I_go 7228034390159984785llu // go
#define I_in_game 11493943715741140089llu // in_game
#define I_lacunarity 15166381217303570937llu // lacunarity
#define I_main 6571252440867548112llu // main
#define I_map_tweaker 7538646738866462232llu // map_tweaker
#define I_octaves 11970457555526361467llu // octaves
#define I_prompt 2784244974187672407llu // prompt
#define I_quit 11228605039341880148llu // quit
#define I_sea_level 13671716229469101616llu // sea_level
#define I_size 2334574115769171026llu // size
#define I_tweaker 9320203585929311420llu // tweaker
#endif
