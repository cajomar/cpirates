#include "game.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "stb_ds.h"
#include "stb_ds_loop.h"

extern float ship_turn_speeds[SHIP_TYPE_END];

// East, West, North, South
bool navigation_buttons_pressed[4] = {false, false, false, false};

typedef struct collision {
    int a, b;
} collision;


static bool ships_collide(const Ship* a, const Ship* b) {
    vec2 diff;
    vec2_sub(diff, a->pos, b->pos);
    float d = vec2_mul_inner(diff, diff);
    if (d < 2.f) {
        return true;
    }
    return false;
}


void game_step(Map* m, float dt) {
    Ship* v = &m->ships[m->players[0].vehicle];
    if (navigation_buttons_pressed[0]) {
        v->turning = -1;
    } else if (navigation_buttons_pressed[1]) {
        v->turning = 1;
    } else {
        v->turning = 0;
    }
    arrforeachp(Ship* s, m->ships) {
        if (s->type == SHIP_TYPE_END) continue;
        if (s->turning) {
            vec2_rot(s->velocity, s->velocity, s->turning * ship_turn_speeds[s->type] * dt);
        }
        vec2 diff;
        vec2_scale(diff, s->velocity, dt);
        vec2_add(s->pos, s->pos, diff);
    }

    collision* ship_collisions = 0; // stretchy_buffer

    arrforeachp(Ship* s, m->ships) {
        if (s->type == SHIP_TYPE_END) continue;
        for (int i=arr_counter+1; i<arrlen(m->ships); i++) {
            Ship* o = &m->ships[i];
            if (o->type == SHIP_TYPE_END) continue;
            assert(s < o);
            if (ships_collide(s, o)) {
                collision c = { arr_counter, i, };
                arrput(ship_collisions, c);
            }
        }
    }
    /*
       sbforeachp(collision* c, ship_collisions) {
       if (m->players->is_human && (m->players->vehicle == *c[0] || 
       m->players->vehicle == *c[1])) {
       }
       }
       */
    arrfree(ship_collisions);
}


bool game_event(Map* m, const struct ink_event* ev) {
    switch (ev->event_type) {
        case INK_EVENT_TYPE_TEXT:
        {
            bool pressed = ev->data.text.key_state != INK_KEY_UP;
            char c = ink_str_char(ev->data.text.text)[0];
            switch (c) {
                case 'a':
                    navigation_buttons_pressed[0] = pressed;
                    return true;
                case 'd':
                    navigation_buttons_pressed[1] = pressed;
                    return true;
            }
            break;
        }
    }
    return false;
}
