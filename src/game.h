#ifndef PIRATES_GAME_H_KTFCH2QA
#define PIRATES_GAME_H_KTFCH2QA

#include <stdbool.h>

#include <ink.h>

#include "map.h"


void game_step(Map* m, float dt);
bool game_event(Map* m, const struct ink_event* ev);

#endif /* end of include guard: PIRATES_GAME_H_KTFCH2QA */
