#include "render.h"

#include <stdbool.h>
#include <assert.h>

#include <ink.h>
#include <lib2d.h>

#include "stb_ds_loop.h"

#define TILE_SIZE 64


extern int viewport[4];
extern float ship_lengths[SHIP_TYPE_END];
extern float ship_beams[SHIP_TYPE_END];

static bool initalized = false;


static lib2d_image* ship_sprite = NULL;

void render_init(void) {
    assert(!initalized);
    initalized = true;
    lib2d_image_info info;
    ship_sprite = lib2d_image_load("data/ship.png", &info);
}

void render_shutdown(void) {
    assert(initalized);
    initalized = false;
    lib2d_image_delete(ship_sprite);
}

void map_tile_to_screen_pixel(float x, float y, float* x_, float* y_) {
    *x_ = x*TILE_SIZE - viewport[0];
    *y_ = y*TILE_SIZE - viewport[1];
}

void render(const Map* m) {
    const Ship* s = &m->ships[m->players->vehicle];
    viewport[0] = s->pos[0]*TILE_SIZE - viewport[2]/2;
    viewport[1] = s->pos[1]*TILE_SIZE - viewport[3]/2;

    for (int y = 0; y < m->size; y++) {
        for (int x = 0; x < m->size; x++) {
            lib2d_drawable d = {
                .w = TILE_SIZE,
                .h = TILE_SIZE,
                .color = m->tiles[y*m->size+x] == MAP_TILE_TYPE_WATER ? \
                         0x44aaffff : 0x44aa44ff,
            };
            map_tile_to_screen_pixel(x, y, &d.x, &d.y);
            lib2d_draw(&d);
        }
    }
    arrforeachp(Ship* ship, m->ships) {
        lib2d_drawable d = {
            .w = ship_lengths[ship->type] * TILE_SIZE,
            .h = ship_beams[ship->type] * TILE_SIZE,
            .color = m->players[ship->player].color,
            .image = ship_sprite,
        };
        map_tile_to_screen_pixel(ship->pos[0], ship->pos[1], &d.x, &d.y);
        d.rot = atan2f(ship->velocity[1], ship->velocity[0]);
        lib2d_draw(&d);
    }
    // Draw minimap
    for (int y = 0; y < m->size; y++) {
        for (int x = 0; x < m->size; x++) {
            lib2d_drawable d = {
                .w = 1,
                .h = 1,
                .color = m->tiles[y*m->size+x] == MAP_TILE_TYPE_WATER ? 0xffff : 0x8800ff,
                .x = x + viewport[2] - m->size,
                .y = y + viewport[3] - m->size,
            };
            lib2d_draw(&d);
        }
    }
}
