#ifndef PIRATES_RENDER_H_MN6U4A5V
#define PIRATES_RENDER_H_MN6U4A5V

#include "map.h"

void render_init(void);
void render_shutdown(void);
void render(const Map* m);

#endif /* end of include guard: PIRATES_RENDER_H_MN6U4A5V */
