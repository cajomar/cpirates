#include "map.h"

#include <stdio.h>

#include <ink_data.h>

#include "idents.h"
#include "stb_ds_loop.h"
#define STB_PERLIN_IMPLEMENTATION
#include "stb_perlin.h"
#include "utils.h"


extern struct ink* ink;


float ship_lengths[SHIP_TYPE_END] = {
    0.5, // SHIP_TYPE_SKIFF
    1.0, // SHIP_TYPE_SLOOP
    1.8, // SHIP_TYPE_MERCHANTMAN
    2.3, // SHIP_TYPE_FRIGATE
    3.0, // SHIP_TYPE_GALLEON
};

float ship_beams[SHIP_TYPE_END] = {
    0.3, // SHIP_TYPE_SKIFF
    0.6, // SHIP_TYPE_SLOOP
    0.9, // SHIP_TYPE_MERCHANTMAN
    1.2, // SHIP_TYPE_FRIGATE
    1.5, // SHIP_TYPE_GALLEON
};

float ship_turn_speeds[SHIP_TYPE_END] = {
    M_PI * 1.0f, // SHIP_TYPE_SKIFF
    M_PI * 0.9f, // SHIP_TYPE_SLOOP
    M_PI * 0.8f, // SHIP_TYPE_MERCHANTMAN
    M_PI * 0.6f, // SHIP_TYPE_FRIGATE
    M_PI * 0.4f, // SHIP_TYPE_GALLEON
};

float ship_max_velocity[SHIP_TYPE_END] = {
    2.f, // SHIP_TYPE_SKIFF
    2.f, // SHIP_TYPE_SLOOP
    2.f, // SHIP_TYPE_MERCHANTMAN
    2.f, // SHIP_TYPE_FRIGATE
    2.f, // SHIP_TYPE_GALLEON
};

void generate_map(Map* m) {
    inkd s = ink_source(ink, I_map_tweaker); 
    float f = inkd_get_float(s, I_divisor);
    float l = inkd_get_float(s, I_lacunarity);
    float g = inkd_get_float(s, I_gain);
    float c = inkd_get_float(s, I_sea_level);
    int o = inkd_get_int(s, I_octaves);

    float min = 10000000000.0;
    float max = -10000000000.0;
    float n[m->size*m->size];
    for (int y = 0; y < m->size; y++) {
        for (int x = 0; x < m->size; x++) {
            float h = stb_perlin_turbulence_noise3(x/f, y/f, 0, l, g, o);
            min = MIN(min, h);
            max = MAX(max, h);
            n[y*m->size+x] = h;
        }
    }
    for (int y = 0; y < m->size; y++) {
        for (int x = 0; x < m->size; x++) {
            float h = (n[y*m->size+x] - min) / (max - min);
            if (h < c) {
                m->tiles[y*m->size+x] = MAP_TILE_TYPE_WATER;
            } else {
                m->tiles[y*m->size+x] = MAP_TILE_TYPE_LAND;
            }
        }
    }
    printf("\n\nmin: %f max: %f\n", min, max);
}

Map* map_create(void) {
    Map* m = calloc(1, sizeof(Map));
    m->size = inkd_get_int(ink_source(ink, I_map_tweaker), I_size);
    m->tiles = malloc(m->size * m->size * sizeof(*m->tiles));
    const char* names[6] = {
        "You",
        "Pirate",
        "England",
        "France",
        "Holland",
        "Spain",
    };
    const uint32_t colors[6] = {
        0xffffffff,
        0x000000ff,
        0xff0000ff,
        0x0000ffff,
        0x00ee00ff,
        0xffff00ff,
    };
    arrsetlen(m->players, 6);
    arrforeachp(Player* p, m->players) {
        p->name = strdup(names[arr_counter]);
        p->color = colors[arr_counter];
        p->is_human = ! arr_counter;
        p->is_pirate = arr_counter < 3;

        int num_ships = p->is_pirate ? 1 : 50;
        Ship* s = arraddnptr(m->ships, num_ships);
        for (int i = 0; i < num_ships; i++, s++) {
            s->pos[0] = rand() % m->size;
            s->pos[1] = rand() % m->size;
            s->type = rand() % SHIP_TYPE_END;
            s->velocity[0] = ship_max_velocity[s->type];
            s->velocity[1] = 0.f;
            vec2_rot(s->velocity, s->velocity, rand() / (float) RAND_MAX * M_PI * 2.f);
            s->player = arr_counter;
            s->turning = 0;
        }
        if (p->is_pirate) {
            p->vehicle_type = VEHICLE_TYPE_SHIP;
            p->vehicle = arrlen(m->ships)-1;
        } else {
            p->vehicle_type = VEHICLE_TYPE_NONE;
        }
    }

    generate_map(m);
    return m;
}

void map_destroy(Map* m) {
    free(m->tiles);
    arrforeachp(Player* p, m->players) free(p->name);
    arrfree(m->players);
    arrfree(m->towns);
    arrfree(m->ships);
    free(m);
}
