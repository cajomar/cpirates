#ifndef PIRATES_CARGO_H_ABJ6LGK2
#define PIRATES_CARGO_H_ABJ6LGK2

#include "mat.h"


enum cargo_type { 
    CARGO_TYPE_CORN=0,
    CARGO_TYPE_INDIGO,
    CARGO_TYPE_SUGAR,
    CARGO_TYPE_TOBACCO,
    CARGO_TYPE_COFFEE,
    CARGO_TYPE_CANNON,
    CARGO_TYPE_GOLD,
    CARGO_TYPE_CREW,
    CARGO_TYPE_END,
};

LINMATH_H_DEFINE_VEC(CARGO_TYPE_END)

typedef vecCARGO_TYPE_END cargo;

#define cargo_add vecCARGO_TYPE_END_add
#define cargo_sub vecCARGO_TYPE_END_sub
#define cargo_scale vecCARGO_TYPE_END_scale
#define cargo_mul_inner vecCARGO_TYPE_END_mul_inner
#define cargo_len vecCARGO_TYPE_END_len
#define cargo_norm vecCARGO_TYPE_END_norm
#define cargo_min vecCARGO_TYPE_END_min
#define cargo_max vecCARGO_TYPE_END_max

#endif /* end of include guard: PIRATES_CARGO_H_ABJ6LGK2 */
