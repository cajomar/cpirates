#ifndef PIRATES_MAP_H_CD3MJFD4
#define PIRATES_MAP_H_CD3MJFD4

#include <stdint.h>

#include "mat.h"
#include "cargo.h"

enum map_tile_type {
    MAP_TILE_TYPE_WATER=0,
    MAP_TILE_TYPE_LAND,
    MAP_TILE_TYPE_END,
};

enum ship_type {
    SHIP_TYPE_SKIFF=0,
    SHIP_TYPE_SLOOP,
    SHIP_TYPE_MERCHANTMAN,
    SHIP_TYPE_FRIGATE,
    SHIP_TYPE_GALLEON,
    SHIP_TYPE_END,
};

enum vehicle_type {
    VEHICLE_TYPE_NONE=0,
    VEHICLE_TYPE_SHIP,
    VEHICLE_TYPE_OVERLAND_PARTY,
    VEHICLE_TYPE_PERSON,
    VEHICLE_TYPE_END,
};

typedef struct player {
    char* name;
    uint32_t color;
    enum vehicle_type vehicle_type;
    int vehicle;
    int is_human : 1;
    int is_pirate : 1;
} Player;

typedef struct ship {
    vec2 pos;
    vec2 velocity;
    enum ship_type type;
    int turning;
    int player;
    cargo cargo;
} Ship;

typedef struct town {
    int x;
    int y;
    cargo cargo;
} Town;

typedef struct map {
    int size;
    enum map_tile_type* tiles;
    Player* players; // stretchy_buffer
    Town* towns; // stretchy_buffer
    Ship* ships; // stretchy_buffer
} Map;

Map* map_create(void);
void map_destroy(Map* m);
void generate_map(Map* m);

#endif /* end of include guard: PIRATES_MAP_H_CD3MJFD4 */
