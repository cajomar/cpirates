#ifndef PIRATES_UTILS_H_Q6KX082C
#define PIRATES_UTILS_H_Q6KX082C

#include <stdbool.h>

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

typedef struct rect {
    int l, t, r, b;
} Rect;

void rect_normalize_inplace(Rect* rect);
void rect_normalize(Rect* out, const Rect* in);
bool rect_contains(Rect*, int x, int y);
bool rect_intersect(const Rect* a, const Rect* b);

#define MIN(a,b) (((a)<(b)) ? (a) : (b))
#define MAX(a,b) (((a)>(b)) ? (a) : (b))

#endif /* end of include guard: PIRATES_UTILS_H_Q6KX082C */
