#include "sea_battle.h"

#include <stdlib.h>

#include "stretchy_buffer.h"

extern bool navigation_buttons_pressed[4];

SeaBattle* sea_battle_create(void) {
    SeaBattle* sb = calloc(1, sizeof(SeaBattle));
    return sb;
}

void sea_battle_destroy(SeaBattle* sb) {
    sbfree(sb->ships);
    free(sb);
}

void sea_battle_step(Map* m, float dt) {
    if (!m->sea_battle) return;
}

bool sea_battle_event(Map* m, const struct ink_event* ev) {
    return false;
}
