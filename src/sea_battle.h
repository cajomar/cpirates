#ifndef SEA_BATTLE_H_H1OEJQ0V
#define SEA_BATTLE_H_H1OEJQ0V

#include <ink.h>

#include "map.h"


SeaBattle* sea_battle_create(void);
void sea_battle_destroy(SeaBattle* sb);
void sea_battle_step(Map* m, float dt);
bool sea_battle_event(Map* m, const struct ink_event* ev);

#endif /* end of include guard: SEA_BATTLE_H_H1OEJQ0V */
