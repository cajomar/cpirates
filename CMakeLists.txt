cmake_minimum_required(VERSION 3.13)
project(PIRATES C)

add_executable(pirates
    src/game.c
    src/main.c
    src/map.c
    src/render.c
    src/utils.c
    )
target_link_libraries(pirates m ink ink_runner lib2d)
target_compile_options(pirates PRIVATE -Wall -Wextra)
