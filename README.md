Caleb Marshall's Pirates
========================
A rewrite in C of my (the [first version](https://bitbucket.org/cajomar/pirates) was in python) pirate game.  
Some things I'm going to change are:  

* No fleets; to can only directly control the ship you're on, although you can still give orders to your other ships and buy/sell their goods with the merchant (if they're in town).  
* Use flocking for the ships.
* A few more things that aren't on the top of my head. When I remember what they are I'll add them here.  

Building
========
```console
# Move into directory
cd cpirates
# Generate build files
cmake -B build
# Build the program
cmake --build build
# Execute the program
./build/pirates
```
